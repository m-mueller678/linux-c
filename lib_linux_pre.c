#include "lib_linux_pre.h"

int handle(int n,int line,char* text){
	if(n<0){
		printf("%3d|%40s|%s\n",line,text,strerror(errno));
		abort();
	}
	return n;
}
