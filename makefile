.DEFAULT_GOAL := run

CC      = /usr/bin/gcc
CFLAGS  = -Wall -Wextra -pedantic -Werror -Wfatal-errors -g
LDFLAGS = -lm -lpthread -lrt

LIB     = $(patsubst %.c,%.o,$(wildcard lib_*.c))
OBJ     = $(patsubst %.c,%.o,$(wildcard *.c))

%.o: %.c
	$(CC) $(CFLAGS) -c $<

all.out: $(OBJ)
	$(CC) -o build.out $(OBJ) $(LDFLAGS) $(CFLAGS)

run: all.out
	./all.out

clean:
	rm $(wildcard *.o) $(wildcard *.out)

single: $(LIB) $(name).o
	$(CC) -o $(name).out $(LIB) $(name).o $(LDFLAGS) $(CFLAGS)

.PHONY: run
.PHONY: clean
.PHONY: single
