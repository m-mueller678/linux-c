#!/bin/bash

name=$1
shift
name=$(awk 'BEGIN{FS=".";OFS="."}{if(NF!=1 && $NF=="c"){ $NF=""; --NF;} print $0}'<<<$name)
if [[ ! -f $name.c ]]
then
	echo '#include "lib_linux_pre.h"' >$name.c
	echo >> $name.c
	echo 'int main(){' >>$name.c
	echo >> $name.c
	echo '}' >> $name.c
fi
nano $name.c
make single name=$name && ( ./$name.out $@ || echo '[exit code:'$?']' )

