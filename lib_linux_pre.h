#ifndef LINUX_C_PRE_H
#define LINUX_C_PRE_H

#include <sysexits.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define H(x) handle(x,__LINE__,#x)

int handle(int n,int line,char* text);

#endif
